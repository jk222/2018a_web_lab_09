$(document).ready(function () {
    // Radio Buttons

    $("input[type=radio]").change(function () {

        // $(this) --- The radio button that was changed
        // $(this).attr("id")
        // radio-background1    Want the bit after the hyphen (background1)
        // Get the background image
        // set its 'src' attribute to '../images/_________.jpg

        var imageBackground = $(this).attr("id").split("-")[1];
        var substringNumber = "6";
        if (imageBackground.indexOf(substringNumber) != -1) {
            $('#background').attr("src", "../images/" + imageBackground + ".gif");
        } else {
            $('#background').attr("src", "../images/" + imageBackground + ".jpg");
        }
    });
});

$(document).ready(function () {

    $("input[type=checkBox]").each(function () {
        var dolphinImage = "#" + $(this).attr("id").split("-")[1];

        if ($(this).is(':checked')) {
            $(dolphinImage).show();
        } else {
            $(dolphinImage).hide();
        }
    });
    //Checkboxes

    $("input[type=checkBox]").change(function () {
        var dolphinImage = "#" + $(this).attr("id").split("-")[1];

        if ($(this).is(':checked')) {
            $(dolphinImage).show();
        } else {
            $(dolphinImage).hide();
        }
    });

    //Range slider

    $("#horizontal-control").change(function () {
        let sliderPosition = $(this)[0].value;

        $(".dolphin").css("left", sliderPosition + "%");


    });

});
